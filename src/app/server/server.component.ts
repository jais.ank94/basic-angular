import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  serverCreationStatus='No server was created';
  serverName='';

  constructor() { }

  ngOnInit() {
  }

  onCreateServer(){
    this.serverCreationStatus='The server is created. Name is '+ this.serverName;
  }

  onUpdateServer(event:Event){
    this.serverName=(<HTMLInputElement>event.target).value;
  }

}
